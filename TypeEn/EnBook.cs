﻿using BaseType;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeEn
{
    [Export("en", typeof(IBook))]
    public class EnBook : IBook
    {
        public string description { get; set; }
        public int index { get; set; }

        public EnBook()
        {
            index = 0;
            description = "english book";
            Console.WriteLine("create instance en");
        }
        public void SayHi()
        {
            index++;
            Console.WriteLine(description + " " + index);
        }
    }
}
