﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MefDemos
{
    class MefUtil
    {
        public static CompositionContainer container { get; private set; }

        public static void LoadTypes()
        {
            AggregateCatalog catalog = new AggregateCatalog();
            container = new CompositionContainer(catalog);
            //扫描对应文件夹下的dll库
            catalog.Catalogs.Add(new DirectoryCatalog(Directory.GetCurrentDirectory()));
            //扫描程序集
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));

        }
    }
}
