﻿using BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
namespace MefDemos
{
    [Export("math", typeof(IBook))]
    public class MathBook : IBook
    {
        public string description { get; set; }
        public int index { get; set; }

        public MathBook()
        {
            index = 0;
            description = "math book";
            Console.WriteLine("create instance math");
        }
        public void SayHi()
        {
            index++;
            Console.WriteLine(description + " " + index);
        }
    }
}
