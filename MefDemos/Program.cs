﻿using BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefDemos
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MefUtil.LoadTypes();
                Console.WriteLine("将TypeCh.dll和TypeEn.dll放在执行文件夹下可正常运行，否则报错");
                MyBooks myBook = MefUtil.container.GetExportedValue<MyBooks>();
                if (myBook != null)
                {
                    myBook.SayHi();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.ReadLine();
        }
    }
}
